import React from 'react'
import { connect } from 'react-redux'
import { ThemeProvider } from 'emotion-theming'
import { dark, light } from './theme/'
import Main from './screens/Main/'

/*
	true: light
	false: dark
*/

const App = ({ mode }) => 
	<ThemeProvider theme={mode ? light : dark}>
		<Main/>
	</ThemeProvider>	

export default connect(state=>({
	mode: state.theme
}))(App)