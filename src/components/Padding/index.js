import styled from 'react-emotion'

const Padding = styled.div`
	padding-left: ${props=>props.left || props.padding}px;
	padding-right: ${props=>props.right || props.padding}px;
	padding-top: ${props=>props.top || props.padding}px;
	padding-bottom: ${props=>props.bottom || props.padding}px;
`

export default Padding