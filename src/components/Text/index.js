import styled from 'react-emotion'

export const H1 = styled.h1`margin-bottom: 0;`
export const H2 = styled.h2`margin-bottom: 0;`
export const H3 = styled.h3`margin-bottom: 0;`
export const H4 = styled.h4`margin-bottom: 0;`
export const H5 = styled.h5`margin-bottom: 0;`
export const H6 = styled.h6`margin-bottom: 0;`
export const Paragraph = styled.p`margin-bottom: 0;`