import styled from 'react-emotion'
import { Input } from 'antd'
const Search = Input.Search

const StyledSearch = styled(Search)`
	outline: none;
	width: 200px;
	.ant-input {
		color: ${props=>props.theme.projectsSearch};
		background-color: transparent;
		border-radius: 20px;
		border: 1px solid ${props=>props.theme.headerColor};
		:focus {
			box-shadow: 0 0 0 2px ${props=>props.theme.projectSearchShadow};
		}
	}
	.anticon {
		color: ${props=>props.theme.projectsSearchIcon};
	}
	:hover, :focus-within {
		.ant-input {
			border-color: ${props=>props.theme.activeHeaderItem}!important;
		}
		.anticon {
			color: ${props=>props.theme.activeHeaderItem}!important;
		}
	}
`

export default StyledSearch