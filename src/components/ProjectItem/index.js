import React from 'react'
import styled from 'react-emotion'
import PropTypes from 'prop-types'
import Flex from '../Flex/'
import { Paragraph } from '../Text/'
import CircleItem from '../CircleItem/'

const Wrapper = styled(Flex)`
	justify-content: space-between;
	padding: 10px 10px;
	margin-bottom: 5px;
	background-color: ${props=>props.theme.projectItemWrapper};
	flex: 1;
`

const ItemDetails = styled.section`
	${Paragraph}:first-of-type {
		color: ${props=>props.theme.text}
	}
	${Paragraph} {
		color: ${props=>props.theme.main}
	}
`

const FavIndicator = styled(CircleItem)`
	background-color: ${props=>props.active ?
		props.theme.active :
		props.theme.inactive
	};
`

const ItemLeft = styled(Flex)`
	${FavIndicator} {
		margin-right: 15px;
	}
`

const ItemRight = styled(Flex)`
	${CircleItem}:not(:first-of-type) {
		margin-left: 10px;
		background-color: ${props=>props.theme.danger};
	}
`

function execute(command) {
	const exec = require('child_process').exec
	console.warn(exec)
	// exec(command, (err, stdout, stderr) => {
	// 	process.stdout.write(stdout)
	// })
}

//ID will be used later...
const ProjectItem = ({ active, name, path, id }) => 
	<Wrapper>
		<ItemLeft>
			<FavIndicator active={active} onClick={()=>execute('echo "Hello World"')}/>
			<ItemLeft>
				<ItemDetails>
					<Paragraph>
						{name}
					</Paragraph>
					<Paragraph>
						{path}
					</Paragraph>
				</ItemDetails>
			</ItemLeft>
		</ItemLeft>
		<ItemRight>
			<CircleItem/>
			<CircleItem/>
		</ItemRight>
	</Wrapper>

ProjectItem.propTypes = {
	active: PropTypes.bool,
	name: PropTypes.string,
	path: PropTypes.string,
	id: PropTypes.number,
}

export default styled(ProjectItem)``