import React from 'react'
import PropTypes from 'prop-types'
import styled from 'react-emotion'

const Indicator = styled.div`
	cursor: pointer;
	background-color: ${props=>
		props.active ? 
			props.theme.toggleActive :
			props.theme.toggleInactive
	};
	border-radius: 50%;
	height: 20px;
	width: 20px;
	transition: .4s;
`

const Switcher = ({ active, ...rest }) =>
	<Indicator active={active} {...rest}/>

Switcher.propTypes = {
	active: PropTypes.bool
}

export default Switcher