import styled from 'react-emotion'
import Flex from '../Flex/'

const Body = styled(Flex)`
	flex: 1 1 0;
	align-self: center;
	justify-content: center;
`

export default Body