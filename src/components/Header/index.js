import styled from 'react-emotion'
import Flex from '../Flex/'
import { H1 } from '../Text'

const Header = styled(Flex)`
	padding-bottom: 5px;
	min-height: 64px;
	background-color: ${props=>props.theme.headerColor};
	${H1} {
		font-weight: 300;
		padding: 20px;
		color: ${props=>props.theme.headerText};
	}
`

export default Header