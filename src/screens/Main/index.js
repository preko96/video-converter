import React from 'react'
import { connect } from 'react-redux'
import { onTestMiddleware } from '../../redux/placeholder/actions'

const Main = ({ onTestMiddleware }) =>
	<div>
		<p>TEST</p>
		<div onClick={onTestMiddleware} style={{
			height: 200,
			width: 200,
			backgroundColor: 'red'
		}}/>
	</div>

export default connect(undefined, dispatch=>({
	onTestMiddleware: () => dispatch(onTestMiddleware())
}))(Main)