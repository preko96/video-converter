import React from 'react'
import { connect } from 'react-redux'
import { onSwitchTheme } from '../../redux/theme/actions'
import Switcher from '../../components/Switcher'

const ThemeSwitcher = ({ active, onSwitchTheme }) =>
	<Switcher active={active} onClick={onSwitchTheme}/>

const mapStateToProps = state => ({
	active: !state.theme
})

const mapDispatchToProps = {
	onSwitchTheme
}

export default connect(
	mapStateToProps, 
	mapDispatchToProps
)(ThemeSwitcher)