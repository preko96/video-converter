import Reactotron, { trackGlobalErrors } from 'reactotron-react-js'
import apisaucePlugin from 'reactotron-apisauce'
import { reactotronRedux } from 'reactotron-redux'
import sagaPlugin from 'reactotron-redux-saga'

const reactotron = Reactotron
	.configure({ name: 'React UI' })
	.use(trackGlobalErrors())
	.use(apisaucePlugin())
	.use(sagaPlugin())
	.use(reactotronRedux())
	.connect()

console.tron = Reactotron

export default reactotron