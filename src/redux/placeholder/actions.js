import { PLACEHOLDER } from 'constants'

export const placeholder = () => ({
	type: PLACEHOLDER
})

export const onTestMiddleware = () => ({
	type: 'ON_TEST_MIDDLEWARE',
	payload: 'LOL',
	meta: {
		electron: true
	}
})