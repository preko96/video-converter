import { createSelector } from 'reselect'

export const selectPlaceholder = state => state.placeholder

export const placeholderSelector = createSelector(
	selectPlaceholder,
	placeholder=>placeholder
)