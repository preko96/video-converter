import { all, takeEvery } from 'redux-saga/effects'
import { PLACEHOLDER } from './constants'

function* placeholderWorker() {
	yield console.tron.log('PLACEHOLDER-SAGA')
}

export default function* rootSaga() {
	yield all([
		takeEvery(PLACEHOLDER, placeholderWorker)
	])
} 
