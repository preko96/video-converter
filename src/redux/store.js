import { combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import { electronMiddleware, startElectronWatchers } from './redux-electron'
import ReactotronConfig from '../ReactotronConfig'
import reducers from './reducers'
import rootSaga from './sagas'
import electronConstants from './electron-constants.js'

const sagaMonitor = ReactotronConfig.createSagaMonitor()
const sagaMiddleware = createSagaMiddleware({ sagaMonitor })

const middlewares = [thunk, sagaMiddleware, electronMiddleware]

const enhancer = compose(
	applyMiddleware(...middlewares)
)

const store = ReactotronConfig.createStore(combineReducers({ ...reducers }), enhancer)
const dispatch = store.dispatch

sagaMiddleware.run(rootSaga)
startElectronWatchers(electronConstants, dispatch)

export default store
