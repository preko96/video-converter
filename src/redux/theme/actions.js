import { ON_SWITCH_THEME } from './constants'

export const onSwitchTheme = () => dispatch => {
	dispatch({
		type: ON_SWITCH_THEME
	})
}