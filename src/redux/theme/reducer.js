import { ON_SWITCH_THEME } from './constants'

//false -> dark
//true -> light

export default (state=false, action={}) => {
	switch(action.type) {
	case ON_SWITCH_THEME:
		return !state
	default:
		return state
	}
}