const { ipcRenderer } = window.require('electron')

const registrateContant = 'Electron/ON_REGISTRATE_WATCHER'

//first param () is the store
export const electronMiddleware = () => dispatch => action => {
	const { payload, type } = action
	if(action.type === registrateContant) {
		ipcRenderer.on(payload, (event, data) => {
			dispatch({
				type: payload,
				data: data
			})
		})
	} 
	else if (action.meta && action.meta.electron) {
		ipcRenderer.send(type, payload)
	}
	// uncomment if want to dispatch the the registration action itself.
	// dispatch(action)
}

export const startElectronWatchers = (watchers, dispatch) => {
	watchers.forEach(watcher=>{
		dispatch({
			type: registrateContant,
			payload: watcher
		})
	})
}