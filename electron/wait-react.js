const net = require('net')

class WaitReact {
  constructor(onConnect, timeout = 500) {
    this.client = new net.Socket()
    this.timeout = timeout
    this.onConnect = onConnect
    this.startedElectron = false
    this.port = process.env.PORT ? process.env.PORT - 100 : 3000
    process.env.ELECTRON_START_URL = `http://localhost:${this.port}` 

    this.onConstruct()
  }

  onConstruct() {
    this.tryConnection()
    this.client.on('error', () => {
      setTimeout(() => this.tryConnection(), this.timeout)
    })
  }

  tryConnection() {
    this.client.connect(
      { port: this.port },
      () => {
        this.client.end()
        if (!this.startedElectron) {
          console.log('starting electron')
          this.startedElectron = true
          if(this.onConnect) {
           this.onConnect()
          } else {
            console.log('onConnect callback is null')
          }
        }
      }
    )
  }
}

module.exports = WaitReact